//Khai báo một thư viện express
const express = require ('express');
//import middlerware
const {userMiddleware} = require("./middlerware");
const {listUser} = require("./data");
// khởi tạo router 
const userRouter = express.Router();
// sử dụng middlerware
userRouter.use(userMiddleware);

userRouter.get("/users",(req,res)=>{
    let age = req.query.Age //cặp key:value trong query string
    console.log(age)
    if (age){
        res.status(200).json({
            users: listUser.filter((item) => item.Age >= age)
        })
    }else{
        res.status(200).json({
            users : listUser
        })
    }
});


userRouter.get("/users/:userId",(req,res)=>{
    let id = req.params.userId;
    console.log(parseInt(id));
    urserid = parseInt(id);
    if(!Number.isInteger(urserid)){
        res.status(400).json({
            message: "Bad Request"
        })
    }
    else{
        res.status(200).json({
            list : listUser.filter((item)=>item.Id == urserid )
        })
    }
})

userRouter.post("/users",(req,res)=>{
    console.log("created a new user");
    res.status(200).json({
        message:"created a new user"
    })
})

userRouter.put("/users/:userId",(req,res)=>{
    let id = req.params.userId;
    console.log("update a new user by id");
    res.status(200).json({
        message: `update a new user by id ${id}`
    })
})

userRouter.delete("/users/:userId",(req,res)=>{
    let id = req.params.userId;
    console.log("delete a new user by id");
    res.status(200).json({
        message: `delete user by id ${id}`
    })
})

module.exports = {userRouter}