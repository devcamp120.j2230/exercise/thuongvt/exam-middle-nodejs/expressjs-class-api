class User {
    constructor( Id,Name,Position,Office,Age,Startdate){
        this.Id = Id,
        this.Name = Name,
        this.Position = Position,
        this.Office = Office,
        this.Age = Age,
        this.Startdate = Startdate
    }
}
let listUser =[];
let id1 = new User("1","Airi Satou","Accountant","Tokyo","33","2008/11/28");
let id2 = new User("2","Angelica Ramos","Chief Executive Officer (CEO)","London","47","2009/10/09");
let id3 = new User("3","Ashton Cox","Junior Technical Author","San Francisco","66","2009/01/12");
let id4 = new User("4","Bradley Greer","Software Engineer","London",'41','2012/10/13');
let id5 = new User( '5','Brenden Wagner','Software Engineer','San Francisco','28','2011/06/07');
let id6 = new User( '6','Brielle Williamson','Integration Specialist','New York','61','2012/12/02');
let id7 = new User( '7','Bruno Nash','Software Engineer','London','38','2011/05/03');
let id8 = new User( '8','Caesar Vance','Pre-Sales Support','New York','21','2011/12/12');
let id9 = new User( '9','Cara Stevens','Sales Assistant','New York','46','2011/12/06');
let id10 = new User( '10','Cedric Kelly','Senior Javascript Developer','Edinburgh','22','2012/03/29');
listUser.push(id1);
listUser.push(id2);
listUser.push(id3);
listUser.push(id4);
listUser.push(id5);
listUser.push(id6);
listUser.push(id7);
listUser.push(id8);
listUser.push(id9);
listUser.push(id10);
// exprot module
module.exports = {listUser}