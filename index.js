const express = require("express"); 
const {userRouter} = require("./router")
const app = express();
const port = 8000;



app.use(userRouter);
app.listen(port, () => {
    console.log(`App đã chạy trên cổng : ${port}`);
})